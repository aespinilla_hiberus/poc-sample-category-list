//
//  BaseList.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct BaseList: Codable {
    let type: String
    let elements: [String]
}
