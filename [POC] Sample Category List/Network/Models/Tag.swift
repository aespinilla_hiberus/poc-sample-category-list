//
//  Tag.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct Tag: Codable {
    let id: Int
    let slug, title, adminTitle: String
    
    enum CodingKeys: String, CodingKey {
        case id, slug, title
        case adminTitle = "admin_title"
    }
}
