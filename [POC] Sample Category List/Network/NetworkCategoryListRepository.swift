//
//  NetworkCategoryListRepository.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import Foundation
import RxSwift

struct NetworkCategoryListRepository: CategoryListRepository {
    
    func fetchCategoryList() -> Single<ResponseCategoryList> {
        if let mock = Mocks.responseCategoryList {
            return Single.just(mock)
                .delay(.seconds(1), scheduler: MainScheduler.instance)
        } else {
            return Single.error(NSError(domain: "sample-category-list", code: 500, userInfo: nil))
        }
    }
    
    func fetchArticleDetail(id: Int) -> Single<ArticleDetail> {
        if let mock = Mocks.readArticle(id: id) {
            return Single.just(mock)
                //.delay(.seconds(1), scheduler: MainScheduler.instance)
        } else {
            return Single.error(NSError(domain: "sample-category-list", code: 500, userInfo: nil))
        }
    }
}
