//
//  RelatedPageComponentView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 4/6/21.
//

import SwiftUI
import Kingfisher

struct RelatedPageComponentView: View {
    var relatedPages: RelatedPageView
    
    var body: some View {
        Link(destination: URL(string: relatedPages.url)!) {
            ZStack {
                KFImage(URL(string: relatedPages.imageUrl))
                    .resizable()
                    .scaledToFit()
                VStack {
                    Spacer()
                    Text(relatedPages.description)
                        .font(.callout)
                        .foregroundColor(.white)
                        .bold()
                        .italic()
                        .background(Color.gray)
                        .opacity(0.8)
                }
            }
        }
    }
}

struct RelatedPageComponentView_Previews: PreviewProvider {
    static var previews: some View {
        RelatedPageComponentView(relatedPages: RelatedPageView(url: "", imageUrl: "", description: ""))
    }
}
