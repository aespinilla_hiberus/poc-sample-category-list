//
//  CategoryListRepository.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import Foundation
import RxSwift

protocol CategoryListRepository {
    func fetchCategoryList() -> Single<ResponseCategoryList>
    func fetchArticleDetail(id: Int) -> Single<ArticleDetail>
}
