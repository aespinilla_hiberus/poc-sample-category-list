//
//  LayoutElement.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

enum LayoutElementType: String, Codable {
    case article = "article"
    case ad = "ad"
}

enum LayoutComponentType: String, Codable {
    case title = "title"
    case epigraphModule = "epigraph-module"
    case multimedia = "multimedia"
    case paragraph = "paragraph"
    case prometeo = "prometeo"
    case ad = "ad"
    case cintillo = "cintillo"
    case baseList = "base-lists"
    case relatedPage = "related_page"
    case compositeCaptionedInlineImage = "composite_captioned_inline_image"
}

struct LayoutElement<T: Codable>: Codable {
    let type: T
    let position: Int
}

//struct LayoutElement: Codable {
//    let type: LayoutElementType
//    let position: Int
//}
