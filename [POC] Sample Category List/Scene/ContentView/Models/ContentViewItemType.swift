//
//  ContentViewItemType.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import Foundation

enum ContentViewItemType {    
    case article(article: ArticleViewModel)
    case ad(ad: AdViewModel)
}

struct ContentViewItem: Identifiable {
    let id = UUID()
    var type: ContentViewItemType
    
    var contentId: Int? {
        switch self.type {
        case .article(let article):
            return article.id
        default:
            return nil
        }
    }
}
