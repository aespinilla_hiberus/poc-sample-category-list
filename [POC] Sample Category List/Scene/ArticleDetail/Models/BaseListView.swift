//
//  BaseListView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 3/6/21.
//

import Foundation

struct BaseListView {
    let type: String
    let elements: [String]
}
