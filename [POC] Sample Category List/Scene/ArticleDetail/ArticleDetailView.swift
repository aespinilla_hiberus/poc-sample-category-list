//
//  ArticleDetailView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import SwiftUI
import Kingfisher

struct ArticleDetailView: View {
    
    @ObservedObject var viewModel: ArticleDetailViewModel
    
    var body: some View {
        ScrollView {
            VStack(spacing: 4) {
                self.headerView
                self.categoriesView
                self.componentsView
                self.pagesTagsView
            }
            .padding()
            .navigationTitle(viewModel.articleModel.title)
            .onAppear(perform: viewModel.setUp)
        }
    }
    
    var headerView: some View {
        VStack(alignment: .leading, spacing: 4) {
            Text(viewModel.articleModel.title)
                .font(.system(.largeTitle, design: .rounded))
                .bold()
                .foregroundColor(.white)
                .background(Color.gray)
            
            HStack {
                Text(viewModel.articleModel.category)
                    .font(.subheadline).bold()
                
                Text(viewModel.articleModel.published)
                    .font(.caption2)
                    .bold()
            }
            
            Text(viewModel.articleModel.author)
                .font(.callout)
                .bold()
        }
    }
    
    var categoriesView: some View {
        HStack {
            ForEach(viewModel.articleModel.categories, id: \.self) { text in
                Text(text)
                    .font(.caption)
                    .foregroundColor(.white)
                    .bold()
                    .padding(.horizontal, 16)
                    .padding(.vertical, 8)
                    .background(Color.gray)
                    .cornerRadius(25)
            }
        }
    }
    
    var componentsView: some View {
        VStack(alignment: .leading, spacing: 4) {
            ForEach(viewModel.articleModel.components) { component in
                self.buildView(component: component)
            }
        }
    }
    
    var pagesTagsView: some View {
        HStack {
            ForEach(viewModel.articleModel.tags, id: \.id) { tag in
                Text(tag.title)
                    .font(.caption)
                    .bold()
                    .padding(8)
                    .cornerRadius(22)
                    .border(Color.black, width: 1)
            }
        }
    }
    
    private func buildView(component: ArticleStyleComponentView) -> AnyView {
        switch component.type {
        case .paragraph(let text):
            return AnyView(ParagraphComponentView(text: text))
        case .multimedia(let multimedia):
            return AnyView(MultimediaComponentView(multimedia: multimedia))
        case .ad(let ad):
            return AnyView(AdCellView(adViewModel: ad))
        case .cintillo(let text):
            return AnyView(CintilloComponentView(text: text))
        case .baseList(let baseList):
            return AnyView(BaseListComponentView(baseListView: baseList))
        case .compositeCaptionedInlineImage(let model):
            return AnyView(CompositeCaptionedInlineImageComponentView(model: model))
        case .epigraphModule(let values):
            return AnyView(EpigraphModuleComponentView(values: values))
        case .relatedPage(let relatedPages):
            if let related = relatedPages {
                return AnyView(RelatedPageComponentView(relatedPages: related))
            } else {
                return AnyView(EmptyView())
            }
        default:
            return AnyView(EmptyView())
        }
    }
}

struct ArticleDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ArticleDetailView(viewModel: ArticleDetailViewModel(id: 4708325))
    }
}
