//
//  CintilloComponentView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 4/6/21.
//

import SwiftUI

struct CintilloComponentView: View {
    var text: String
    
    var body: some View {
        Text(text)
            .font(.title2)
            .bold()
            .italic()
    }
}

struct CintilloComponentView_Previews: PreviewProvider {
    static var previews: some View {
        CintilloComponentView(text: "Título de ejemplo")
    }
}
