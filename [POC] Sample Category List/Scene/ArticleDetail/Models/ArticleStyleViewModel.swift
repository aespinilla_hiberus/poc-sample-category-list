//
//  ArticleStyleViewModel.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 3/6/21.
//

import Foundation

struct ArticleStyleViewModel {
    var title: String
    var category: String
    var categories: [String]
    var author: String
    var published: String
    var tags: [TagView] = []
    var components: [ArticleStyleComponentView] = []
}

struct ArticleStyleComponentView: Identifiable {
    let id = UUID()
    let type: ArticleStyleType
}

extension ArticleStyleViewModel {
    static var empty: ArticleStyleViewModel {
        return ArticleStyleViewModel(title: "",
                                     category: "",
                                     categories: [],
                                     author: "",
                                     published: "")
    }
}
