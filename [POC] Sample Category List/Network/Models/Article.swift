//
//  Article.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct Article: Codable {
    let id: Int
    let title: String
    let authors: [Author]
    let imageURL: String
    let slug, accessType, formatType: String

    enum CodingKeys: String, CodingKey {
        case id, title, authors
        case imageURL = "image_url"
        case slug, accessType, formatType
    }
}
