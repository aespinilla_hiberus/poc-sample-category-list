//
//  ArticleStyle.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 3/6/21.
//

import Foundation

enum ArticleStyleType {
    case title(String)
    case epigraphModule([EpigraphModuleView])
    case multimedia(MultimediaView)
    case paragraph(String)
    case prometeo(String)
    case ad(AdViewModel)
    case cintillo(String)
    case baseList(BaseListView)
    case relatedPage(RelatedPageView?)
    case compositeCaptionedInlineImage(CompositeCaptionedInlineImageView)
}
