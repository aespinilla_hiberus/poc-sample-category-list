//
//  Multimedia.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct Multimedia: Codable {
    let imageUrl: String
    let description: String
    
    let credits: String?
    
    let id: Int?
    let title: String?
    let source: String?
    let fileUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case credits, description, id, title, source
        case imageUrl = "image_url"
        case fileUrl = "file_url"
    }
}
