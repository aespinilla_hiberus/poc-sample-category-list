//
//  AdViewModel.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import Foundation

struct AdViewModel {
    let adUnitID, adSizes: String
    let customTargeting: [KeyValueModel]
}

struct KeyValueModel {
    let key, value: String
}
