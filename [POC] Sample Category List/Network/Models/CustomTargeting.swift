//
//  CustomTargeting.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct CustomTargeting: Codable {
    let key, value: String
}
