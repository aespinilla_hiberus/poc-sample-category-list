//
//  RelatedPage.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct RelatedPage: Codable {
    let url: String
    let imageUrl: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case url, description
        case imageUrl = "image_url"
    }
}
