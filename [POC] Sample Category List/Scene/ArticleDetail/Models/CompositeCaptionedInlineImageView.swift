//
//  CompositeCaptionedInlineImageView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 3/6/21.
//

import Foundation

struct CompositeCaptionedInlineImageView {
    let imageUrl: String
    let credits: String
    let description: String
}
