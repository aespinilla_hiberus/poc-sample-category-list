//
//  MultimediaComponentView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 4/6/21.
//

import SwiftUI
import AVKit
import Kingfisher

struct MultimediaComponentView: View {
    var multimedia: MultimediaView
    
    var body: some View {
        if let fileUrl = multimedia.fileUrl {
            VideoPlayer(player: AVPlayer(url: URL(string: fileUrl)!))
                .frame(height: 400)
        } else {
            KFImage(URL(string: multimedia.imageUrl))
                .resizable().frame(height: 180)
        }
        
    }
}

struct MultimediaComponentView_Previews: PreviewProvider {
    static var previews: some View {
        MultimediaComponentView(multimedia: MultimediaView(imageUrl: "",
                                                           description: "",
                                                           credits: nil,
                                                           id: nil,
                                                           title: nil,
                                                           source: nil,
                                                           fileUrl: nil))
    }
}
