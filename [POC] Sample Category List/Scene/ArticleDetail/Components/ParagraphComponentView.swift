//
//  ParagraphComponentView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 4/6/21.
//

import SwiftUI

struct ParagraphComponentView: View {
    var text: String
    
    var body: some View {
        Text(text)
            .font(.body)
    }
}

struct ParagraphComponentView_Previews: PreviewProvider {
    static var previews: some View {
        ParagraphComponentView(text: "Lorem ipsum")
    }
}
