//
//  Mock.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import Foundation

struct Mocks {
    
    static var responseCategoryList: ResponseCategoryList? {
        guard let json = self.readFile(name: "mock-response-category-list"), let data = json.data(using: .utf8) else { return nil }
        let decoder = JSONDecoder()
        return try? decoder.decode(ResponseCategoryList.self, from: data)
    }
    
    private static func readFile(name: String) -> String? {
        guard let filePath = Bundle.main.path(forResource: name, ofType: "json") else { return nil }
        let value = try? String(contentsOfFile: filePath, encoding: .utf8)
        return value
    }
    
    static func readArticle(id: Int) -> ArticleDetail? {
        guard let json = self.readFile(name: "noticia-\(id)"), let data = json.data(using: .utf8) else { return nil }
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(ArticleDetail.self, from: data)
        } catch let error {
            print(error)
        }
        return nil
    }
}
