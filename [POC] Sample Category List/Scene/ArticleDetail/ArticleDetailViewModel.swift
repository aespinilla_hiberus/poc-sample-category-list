//
//  ArticleDetailViewModel.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation
import RxSwift
import SwiftUI
import SwiftSoup

class ArticleDetailViewModel: ObservableObject {
    
    @Published var articleModel: ArticleStyleViewModel = .empty
    
    private let repository: CategoryListRepository
    private let articleId: Int
    private let globalScheduler = SerialDispatchQueueScheduler(queue: .global(), internalSerialQueueName: "background-fetch-detail")
    private let disposeBag = DisposeBag()
    
    init(id: Int, repository: CategoryListRepository = NetworkCategoryListRepository()) {
        self.repository = repository
        self.articleId = id
        self.setUp()
    }
    
    func setUp() {
        self.fetchDetail()
    }
    
    private func fetchDetail() {
        repository.fetchArticleDetail(id: self.articleId)
            .observe(on: globalScheduler) // Ejecutamos la petición en otro hilo
            .subscribe(on: MainScheduler.instance) // [Opcional] Observamos en el hilo principal
            .subscribe { [weak self] details in
                guard let self = self else { return }
                // Actualizamos en el hilo principal
                DispatchQueue.main.async {
                    //print(details)
                    self.articleModel = self.transform(article: details)
                }
            } onFailure: { error in
                print(error.localizedDescription)
            }
            .disposed(by: disposeBag)
    }
    
    private func transform(article: ArticleDetail) -> ArticleStyleViewModel {
        var viewModel = ArticleStyleViewModel(title: article.title,
                                              category: article.category.title,
                                              categories: article.categories.map { $0.title },
                                              author: article.authors.map { $0.fullName }.joined(separator: ","),
                                              published: formatDate(date: article.publishedAt),
                                              tags: article.pageTags.map { TagView(id: $0.id, title: $0.title) })
        let adMapper = AdViewMapper()
        viewModel.components = article.layout.map { element in
            switch element.type {
            case .title:
                return ArticleStyleComponentView(type: .title(article.titles[element.position]))
            case .epigraphModule:
                let items = article.epigraphModules[element.position].map { EpigraphModuleView(text: $0.text, url: $0.url) }
                return ArticleStyleComponentView(type: .epigraphModule(items))
            case .multimedia:
                let item = article.multimedia[element.position]
                return ArticleStyleComponentView(type: .multimedia(MultimediaView(imageUrl: item.imageUrl,
                                                                           description: item.description,
                                                                           credits: item.credits,
                                                                           id: item.id,
                                                                           title: item.title,
                                                                           source: item.source,
                                                                           fileUrl: item.fileUrl)))
                
            case .paragraph:
                return ArticleStyleComponentView(type: .paragraph(parseText(source: article.paragraphs[element.position])))
            case.prometeo:
                return ArticleStyleComponentView(type: .prometeo(article.prometeo[element.position]))
            case .ad:
                return ArticleStyleComponentView(type: .ad(adMapper.map(article.ads[element.position])))
            case .cintillo:
                return ArticleStyleComponentView(type: .cintillo(article.cintillos[element.position]))
            case .baseList:
                let item = article.baseLists[element.position]
                return ArticleStyleComponentView(type: ArticleStyleType.baseList(BaseListView(type: item.type,
                                                                                              elements: item.elements.map { parseText(source: $0) })))
            case .relatedPage:
                
                if let item = article.relatedPages?[element.position] {
                    return ArticleStyleComponentView(type: .relatedPage(RelatedPageView(url: item.url,
                                                                                 imageUrl: item.imageUrl,
                                                                                 description: item.description)))
                }
                return ArticleStyleComponentView(type: .relatedPage(nil))
            case .compositeCaptionedInlineImage:
                let item = article.compositeCaptionedInlineImage[element.position]
                let type: ArticleStyleType = .compositeCaptionedInlineImage(CompositeCaptionedInlineImageView(imageUrl: item.imageUrl,
                                                                                            credits: item.credits,
                                                                                            description: item.description))
                return ArticleStyleComponentView(type: type)
            }
        }
        return viewModel
    }
    
    private func parseText(source: String) -> String {
        do {
           let doc: Document = try SwiftSoup.parse(source)
           return try doc.text()
        } catch Exception.Error( _, let message) {
            print(message)
        } catch {
            print("error")
        }
        return source
    }
    
    private func formatDate(date: String) -> String {
        var formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let parsedDate = formatter.date(from: date) else { return date }
        formatter = DateFormatter()
        formatter.dateFormat = "HH:mm 'del' dd/MM/yyyy"
        return formatter.string(from: parsedDate)
    }
}
