//
//  ContentViewMapper.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 31/5/21.
//

import Foundation

struct ArticleViewMapper: BaseMapper {
    typealias O = Article
    typealias E = ArticleViewModel
    
    func map(_ origin: Article) -> ArticleViewModel {
        return ArticleViewModel(id: origin.id,
                                title: origin.title,
                                authors: origin.authors.map { $0.fullName },
                                imageURL: origin.imageURL,
                                slug: origin.slug,
                                accessType: origin.accessType,
                                formatType: origin.formatType)
    }
}

struct AdViewMapper: BaseMapper {
    typealias O = Ad
    typealias E = AdViewModel
    
    func map(_ origin: Ad) -> AdViewModel {
        return AdViewModel(adUnitID: origin.adUnitID,
                           adSizes: origin.adSizes,
                           customTargeting: origin.customTargeting.map { KeyValueModel(key: $0.key, value: $0.value) })
    }
}
