//
//  BaseMapper.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 31/5/21.
//

import Foundation

protocol BaseMapper {
    
    associatedtype O
    associatedtype E
    
    func map(_ origin: O) -> E
}
