//
//  EpigraphModule.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct EpigraphModule: Codable {
    let list: Bool
    let text: String
    let url: String?
}
