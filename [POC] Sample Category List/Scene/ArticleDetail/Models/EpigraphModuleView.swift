//
//  EpigraphModuleView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 3/6/21.
//

import Foundation

struct EpigraphModuleView {
    let text: String
    let url: String?
}
