//
//  Ad.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct Ad: Codable {
    let adUnitID, adSizes: String
    let customTargeting: [CustomTargeting]

    enum CodingKeys: String, CodingKey {
        case adUnitID = "adUnitId"
        case adSizes, customTargeting
    }
}
