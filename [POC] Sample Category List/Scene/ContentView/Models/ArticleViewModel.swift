//
//  ArticleViewModel.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import Foundation

struct ArticleViewModel {
    let id: Int
    let title: String
    let authors: [String]
    let imageURL: String
    let slug, accessType, formatType: String
    
    var allAuthors: String {
        self.authors.joined()
    }
}
