//
//  Category.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct Category: Codable {
    let id: Int
    let slug: String
    let title: String
}
