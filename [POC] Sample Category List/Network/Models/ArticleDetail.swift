//
//  ArticleDetail.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct ArticleDetail: Codable {
    let id: Int
    let category: Category
    let slug: String
    let accessType: String
    let authors: [Author]
    let categories: [Category]
    let pageTags: [Tag]
    
    let createdAt: String
    let updateAt: String
    let publishedAt: String
    
    let title: String
    let contributingAuthorSetting: String
    let authorsTags: [Tag]
    let template: String
    let publish: Bool
    
    var titles: [String] = []
    var epigraphModules: [[EpigraphModule]] = []
    var multimedia: [Multimedia] = []
    var paragraphs: [String] = []
    var relatedPages: [RelatedPage]?
    var ads: [Ad] = []
    var prometeo: [String] = []

    var cintillos: [String] = []
    
    var baseLists: [BaseList] = []
    var compositeCaptionedInlineImage: [CompositeCaptionedInlineImage] = []
    
    var layout: [LayoutElement<LayoutComponentType>] = []
    let url: String
    
    enum CodingKeys: String, CodingKey {
        case id, category, slug, accessType, authors, categories, pageTags
        case title, contributingAuthorSetting, template, publish, titles, paragraphs
        case multimedia, layout, prometeo, ads, url, cintillos
        case createdAt = "created_at"
        case updateAt = "updated_at"
        case publishedAt = "published_at"
        case authorsTags = "authors_tags"
        case epigraphModules = "epigraph-modules"
        case relatedPages = "related_pages"
        case baseLists = "base-lists"
        case compositeCaptionedInlineImage = "composite_captioned_inline_image"
    }
}
