//
//  TagView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 4/6/21.
//

import Foundation

struct TagView {
    let id: Int
    let title: String
}
