//
//  Author.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct Author: Codable {
    let id: Int
    let username, firstName, lastName: String

    enum CodingKeys: String, CodingKey {
        case id, username
        case firstName = "first_name"
        case lastName = "last_name"
    }
    
    var fullName: String {
        return firstName + " " + lastName
    }
}
