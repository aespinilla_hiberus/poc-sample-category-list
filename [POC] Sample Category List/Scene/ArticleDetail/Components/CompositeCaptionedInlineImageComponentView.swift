//
//  CompositeCaptionedInlineImageComponentView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 4/6/21.
//

import SwiftUI
import Kingfisher

struct CompositeCaptionedInlineImageComponentView: View {
    var model: CompositeCaptionedInlineImageView
    
    var body: some View {
        VStack(alignment: .center) {
            KFImage(URL(string: model.imageUrl))
                .resizable()
                .scaledToFit()
            Text(model.description)
                .font(.callout)
                .bold()
                .italic()
                .frame(alignment: .center)
        }
    }
}

struct CompositeCaptionedInlineImageComponentView_Previews: PreviewProvider {
    static var previews: some View {
        CompositeCaptionedInlineImageComponentView(model: CompositeCaptionedInlineImageView(imageUrl: "", credits: "", description: ""))
    }
}
