//
//  ContentViewInteractor.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 31/5/21.
//

import Foundation
import RxSwift

protocol ContentViewInteractor {
    func getCategoryList(categoryId: Int) -> Single<[ContentViewItem]>
}

class ContentViewInteractorImpl: ContentViewInteractor {
    
    private let repository: CategoryListRepository
    
    private let articleMapper = ArticleViewMapper()
    private let adMapper = AdViewMapper()
    
    init(repository: CategoryListRepository = NetworkCategoryListRepository()) {
        self.repository = repository
    }
    
    func getCategoryList(categoryId: Int) -> Single<[ContentViewItem]> {
        repository.fetchCategoryList()
            .map(self.transform)
    }
    
    private func transform(origin: ResponseCategoryList) -> [ContentViewItem] {
        let articles = origin.articles
        let ads = origin.ads
        
        return origin.layout.compactMap {
            switch $0.type {
            case .ad:
                let type: ContentViewItemType = .ad(ad: self.adMapper.map(ads[$0.position]))
                return ContentViewItem(type: type)
            case .article:
                let type: ContentViewItemType = .article(article: self.articleMapper.map(articles[$0.position]))
                return ContentViewItem(type: type)
            }
        }
    }
}
