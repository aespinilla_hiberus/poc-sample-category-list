//
//  MultimediaView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 3/6/21.
//

import Foundation

struct MultimediaView {
    let imageUrl: String
    let description: String
    
    let credits: String?
    
    let id: Int?
    let title: String?
    let source: String?
    let fileUrl: String?
    
    var isVideo: Bool {
        self.fileUrl != nil
    }
}
