//
//  RelatedPageView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 3/6/21.
//

import Foundation

struct RelatedPageView {
    let url: String
    let imageUrl: String
    let description: String
}
