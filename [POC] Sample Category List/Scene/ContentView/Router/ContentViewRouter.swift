//
//  ContentViewRouter.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

protocol ContentViewRouterProtocol {
    func navigateToDetail(id: Int) -> ArticleDetailView
}

class ContentViewRouter: ContentViewRouterProtocol {
    
    func navigateToDetail(id: Int) -> ArticleDetailView {
        ArticleDetailView(viewModel: ArticleDetailViewModel(id: id))
    }
}
