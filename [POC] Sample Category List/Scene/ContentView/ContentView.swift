//
//  ContentView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import SwiftUI
import Kingfisher

struct ContentView: View {
    
    @ObservedObject var viewModel = ContentViewModel()
    
    var body: some View {
        NavigationView {
            List(self.viewModel.items) { item in
                if let id = item.contentId {
                    NavigationLink(destination: viewModel.router.navigateToDetail(id: id)) {
                        CellView(contentViewItem: item)
                    }
                } else {
                    CellView(contentViewItem: item)
                }
            }.navigationTitle("Category List")
            .onAppear(perform: self.viewModel.fetchCategoryList)
        }
    }
//    
//    func createCell(contentViewItem: ContentViewItem) -> AnyView {
//        switch contentViewItem.type {
//        case .article(let article):
//            return ArticleCellView(articleViewModel: article)
//        case .ad(let ad):
//            return AdCellView(adViewModel: ad)
//        }
//    }
}

struct CellView: View {
    var contentViewItem: ContentViewItem
    
    var body: some View {
//        NavigationLink(destination: viewModel.router.navigateToDetail(id: item.)) {
//            CellView(contentViewItem: item)
//        }
        switch contentViewItem.type {
        case .article(let article):
            ArticleCellView(articleViewModel: article)
        case .ad(let ad):
            AdCellView(adViewModel: ad)
        }
    }
}

struct ArticleCellView: View {
    
    var articleViewModel: ArticleViewModel
    
    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 4) {
                KFImage(URL(string: articleViewModel.imageURL))
                    .resizable()
                    .scaledToFit()
                    .frame(height: 200)
                    .cornerRadius(12)
                
                Text(articleViewModel.title)
                    .font(.headline)
                
                Text(articleViewModel.allAuthors)
                    .font(.subheadline)
            }
        }
    }
}

struct AdCellView: View {
    
    var adViewModel: AdViewModel
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text("AdUnit: \(adViewModel.adUnitID)")
                    .foregroundColor(.white)
                Text("Value: \(adViewModel.customTargeting.first?.value ?? "")")
                    .foregroundColor(.white)
                Text("Sizes: \(adViewModel.adSizes)")
                    .foregroundColor(.white)
            }
        }
        .frame(maxWidth: .infinity, minHeight: 180, idealHeight: 180, maxHeight: 180)
        .background(Color.gray)
        .cornerRadius(12)
        .overlay(
            RoundedRectangle(cornerRadius: 12)
                .stroke(Color.gray, lineWidth: 2)
        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
