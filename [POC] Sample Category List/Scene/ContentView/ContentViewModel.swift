//
//  ContentViewModel.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import Foundation
import RxSwift
import Combine

class ContentViewModel: ObservableObject {
    
    @Published var items: [ContentViewItem] = []
    
    private let interactor: ContentViewInteractor
    
    let router: ContentViewRouterProtocol
    
    private let disposeBag = DisposeBag()
    
    private let globalScheduler = SerialDispatchQueueScheduler(queue: DispatchQueue.global(), internalSerialQueueName: "background-fetch")
    
    init(interactor: ContentViewInteractor = ContentViewInteractorImpl(), router: ContentViewRouterProtocol = ContentViewRouter()) {
        self.interactor = interactor
        self.router = router
    }
    
    func fetchCategoryList() {
        interactor.getCategoryList(categoryId: 1)
            .observe(on: globalScheduler) // Ejecutamos la petición en otro hilo
            .subscribe(on: MainScheduler.instance) // [Opcional] Observamos en el hilo principal
            .subscribe { [weak self] items in
                
                // Actualizamos en el hilo principal
                DispatchQueue.main.async {
                    self?.items = items
                }
            } onFailure: { error in
                print(error.localizedDescription)
            }
            .disposed(by: disposeBag)
    }
}
