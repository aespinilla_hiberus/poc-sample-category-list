//
//  ResponseCategoryList.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 28/5/21.
//

import Foundation

// MARK: - ResponseCategoryList
struct ResponseCategoryList: Codable {
    let total: Int
    let hasMoreItems: Bool
    let page: Int
    let articles: [Article]
    let ads: [Ad]
    let layout: [LayoutElement<LayoutElementType>]
}
