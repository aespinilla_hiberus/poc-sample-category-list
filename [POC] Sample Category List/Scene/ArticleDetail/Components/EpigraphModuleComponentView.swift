//
//  EpigraphModuleComponentView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 4/6/21.
//

import SwiftUI

struct EpigraphModuleComponentView: View {
    var values: [EpigraphModuleView]
    
    var body: some View {
        VStack(alignment: .leading, spacing: 2) {
            ForEach(values, id: \.url) { item in
                HStack(alignment: .top) {
                    Image(systemName: "link.circle.fill")
                        .font(.caption)
                        .foregroundColor(.black)
                    
                    if let url = item.url {
                        Link(destination: URL(string: url)!) {
                            Text(item.text)
                                .font(.caption)
                                .bold()
                                .italic()
                                .underline()
                        }
                    } else {
                        Text(item.text)
                            .font(.caption)
                            .bold()
                            .italic()
                            .underline()
                    }
                }
            }
        }
    }
}

struct EpigraphModuleComponentView_Previews: PreviewProvider {
    static var previews: some View {
        EpigraphModuleComponentView(values: [])
    }
}
