//
//  BaseListComponentView.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 4/6/21.
//

import SwiftUI

struct BaseListComponentView: View {
    var baseListView: BaseListView
    
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(baseListView.elements, id: \.self) { item in
                HStack(alignment: .top) {
                    Image(systemName: "hand.point.right.fill")
                        .font(.caption)
                        .foregroundColor(.white)
                    
                    Text(item)
                        .font(.caption)
                        .italic()
                        .foregroundColor(.white)
                }
                
            }
        }
        .background(Color.gray)
    }
}

struct BaseListComponentView_Previews: PreviewProvider {
    static var previews: some View {
        BaseListComponentView(baseListView: BaseListView(type: "ul", elements: []))
    }
}
