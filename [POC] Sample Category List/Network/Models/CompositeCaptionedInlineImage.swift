//
//  CompositeCaptionedInlineImage.swift
//  [POC] Sample Category List
//
//  Created by Alberto Espinilla  on 2/6/21.
//

import Foundation

struct CompositeCaptionedInlineImage: Codable {
    let imageUrl: String
    let credits: String
    let description: String
    
    enum CodingKeys: String, CodingKey {
        case credits, description
        case imageUrl = "image_url"
    }
}
